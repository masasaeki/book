FROM ruby:2.6

RUN apt-get update -qq && apt-get install -y build-essential


RUN mkdir -p /myapp
WORKDIR /myapp
COPY ./Gemfile /myapp/Gemfile
COPY ./Gemfile.lock /myapp/Gemfile.lock
RUN gem install bundler
RUN bundle install
COPY . /myapp/



# Gemfile
# gem 'rspec'
# source 'https://rubygems.org'


# docker build -t sasasasa/ruby .
# docker run --name ruby -it -v $PWD:/myapp sasasasa/ruby
# docker ps -a
# docker stop ruby
# docker start ruby
# docker exec -it ruby /bin/bash
