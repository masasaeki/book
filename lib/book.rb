class Book
  include Comparable
  
  def <=>(other)
    t = @genre <=> other.genre
    return t if t != 0
    return @title <=> other.title
  end

  attr_accessor :title, :author, :genre
  def initialize(title, author, genre=nil)
    @title = title
    @author = author
    @genre = genre
  end
end

class BookList
  include Enumerable

  def initialize
    @booklist = []
  end
  def add(book)
    @booklist.push(book)
  end
  def length
    @booklist.length
  end
  def [](n)
    @booklist[n]
  end
  def []=(n, book)
    @booklist[n] = book
  end
  def delete(book)
    @booklist.delete(book)
  end
  def each
    @booklist.each do |book|
      yield(book)
    end
  end
  def each_title
    each do |book| 
      yield(book.title)
    end
  end
  def each_title_author
    each do |book| 
      yield(book.title, book.author)
    end
  end

  def find_by_author(author)
    raise ArgumentError unless author.instance_of?(Regexp)
    if block_given?
      each do |book|
        if author =~ book.author
          yield(book)
        end
      end
    else
      result = []
      each do |book|
        if author =~ book.author
          result << book
        end
      end
      return result
    end
  end
end

if __FILE__ == $0
  booklist = BookList.new
  b1 = Book.new("せめて、本格らしく", "上閉胸", "SF")
  b2 = Book.new("AbcなSF", "かとう", "SF")
  b3 = Book.new("Neo Aqua III", "Neo Aqua", "Adventure")
  b4 = Book.new("PPPp", "グレッグ", "Fiction")
  booklist.add(b1)
  booklist.add(b2)
  booklist.add(b3)
  booklist.add(b4)

  print booklist[0].title, "\n"
  print booklist[1].title, "\n"

  puts "----------------------------------"

  booklist.each do |book|
    print book.title, "\n"
  end

  puts "----------------------------------"

  booklist.each_title do |title|
    print title, "\n"
  end

  puts "----------------------------------"

  booklist.each_title_author do |title, author|
    print "「#{title}」, #{author} \n"
  end

  puts "------find_by_author----------------------------"

  booklist.find_by_author(/グレッグ/) do |book|
    print book.title, "#{book.author} \n"
  end

  books = booklist.find_by_author(/上閉胸/)
  books.each do |book|
    print book.title, "#{book.author} \n"
  end

  begin
    booklist.find_by_author("Hoge")
  rescue => exception
    puts exception
  end

  puts "-----sort-----------------------------"

  booklist.sort.each do |book|
    print "「#{book.title}」,#{book.genre} \n"
  end

  puts "-----map-----------------------------"

  titles = booklist.map do |book|
    book.title
  end
  puts titles

  puts "-----zip-----------------------------"
  booklist.zip([1,2,3,4]) do |book, num|
    print "#{book.title} - #{num}", "\n"
  end
end