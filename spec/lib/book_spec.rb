require "spec_helper"
require "book"


describe BookList do
  let(:book1) { 
    title = "テストタイトル1"
    author = "テストオーサー1"
    genre = "adventure"
    book = Book.new(title, author, genre)
  }
  let(:book2) { 
    title = "テストタイトル2"
    author = "テストオーサー2"
    genre = "sf"
    book = Book.new(title, author, genre)
  }
  let(:book3) { 
    title = "テストタイトル3"
    author = "テストオーサー3"
    genre = "sf"
    book = Book.new(title, author, genre)
  }
  let(:book4) { 
    title = "テストタイトル4"
    author = "テストオーサー4"
    genre = "fiction"
    book = Book.new(title, author, genre)
  }
  let(:booklist) { BookList.new() }
  before(:each) do
    booklist.add(book1)
    booklist.add(book2)
    booklist.add(book3)
    booklist.add(book4)
  end

  describe "length" do
    it "add した数だけ length が増える" do
      expect(booklist.length).to eq 4
      booklist.add(Book.new("title","author","genre"))
      expect(booklist.length).to eq 5
    end

    it "add した数だけ length が増える 別version" do
      expect{ 
        booklist.add(Book.new("title","author","genre"))
      }.to change{ booklist.length }.from(4).to(5)
    end

    it "delete した数だけ length が減る" do
      expect{ 
        booklist.delete(book1)
      }.to change{ booklist.length }.from(4).to(3)
    end
  end

  describe "[]" do
    it "add した順番で取り出せる" do
      expect(booklist[0].title).to eq "テストタイトル1"
      expect(booklist[0].author).to eq "テストオーサー1"
      expect(booklist[0].genre).to eq "adventure"
    end
  end

  describe "[]=" do
    it "指定した順番の要素を変更できる" do
      expect(booklist[0].title).to eq "テストタイトル1"
      expect(booklist[0].author).to eq "テストオーサー1"
      expect(booklist[0].genre).to eq "adventure"
      booklist[0] = Book.new("test title", "test author", "test genre")
      expect(booklist[0].title).to eq "test title"
      expect(booklist[0].author).to eq "test author"
      expect(booklist[0].genre).to eq "test genre"
    end
  end

  describe "delete" do
    it "指定した要素を削除できる" do
      expect(booklist[0].title).to eq "テストタイトル1"
      expect(booklist[0].author).to eq "テストオーサー1"
      expect(booklist[0].genre).to eq "adventure"
      booklist.delete(book1)
      expect(booklist[0].title).to_not eq "テストタイトル1"
      expect(booklist[0].author).to_not eq "テストオーサー1"
      expect(booklist[0].genre).to_not eq "adventure"
    end
  end

  describe "each (Enumerable)" do
    it "add した要素を追加した順番通りに全て得ることが出来る" do
      booklist.zip([book1,book2,book3,book4]) do |exp,act|
        expect(exp).to eq act
      end
    end
  end

  describe "sort (Enumerable)" do
    it "ジャンル、タイトルの順番でソートされること" do
      books = booklist.sort
      expect(books).to eq [
        book1,
        book4,
        book2,
        book3,
      ]
    end
  end

  describe "each_title" do
    it "追加した順番通りにタイトルを得ることが出来る" do
      titles = [
        "テストタイトル1",
        "テストタイトル2",
        "テストタイトル3",
        "テストタイトル4",
      ]
      i = 0
      booklist.each_title do |title|
        expect(title).to eq titles[i]
        i += 1
      end
    end
  end

  describe "each_title_author" do
    it "追加した順番通りにタイトルと作者を得ることが出来る" do
      titles = [
        "テストタイトル1",
        "テストタイトル2",
        "テストタイトル3",
        "テストタイトル4",
      ]
      authors = [
        "テストオーサー1",
        "テストオーサー2",
        "テストオーサー3",
        "テストオーサー4",
      ]
      i = 0
      booklist.each_title_author do |title, author|
        expect(title).to eq titles[i]
        expect(author).to eq authors[i]
        i += 1
      end
    end
  end

  describe "find_by_author" do
    it "正規表現以外を引数に渡すとエラーになる" do
      expect { 
        booklist.find_by_author("Hoge")
      }.to raise_error ArgumentError
    end

    describe "block given" do
      it "引数に渡した作者の本をブロック引数に返す" do
        booklist.find_by_author(/テストオーサー2/) do |book|
          expect(book.title).to eq "テストタイトル2"
        end
      end
    end

    describe "block not given" do
      it "引数に渡した作者の本を配列で返す" do
        books = booklist.find_by_author(/テストオーサー3/)
        expect(books.pop.title).to eq "テストタイトル3"
      end
    end
  end
end

describe Book do
  describe "new" do
    let(:book) { 
      title = "テストタイトル"
      author = "テストオーサー"
      genre = "テストジャンル"
      book = Book.new(title, author, genre)
    }

    it "引数をすべて指定する" do
      expect(book.title).to eq "テストタイトル"
      expect(book.author).to eq "テストオーサー"
      expect(book.genre).to eq "テストジャンル"
    end

    it "引数を省略する" do
      title = "テストタイトル"
      author = "テストオーサー"
      book = Book.new(title, author)
      expect(book.title).to eq title
      expect(book.author).to eq author
      expect(book.genre).to eq nil
    end
  end

  describe "<=>" do
    it "ジャンルで並ぶこと" do
      b1 = Book.new("1","1","a")
      b2 = Book.new("1","1","b")
      expect(b1).to be < b2
    end

    it "ジャンルで並ぶこと" do
      b1 = Book.new("1","1","a")
      b2 = Book.new("1","1","b")
      expect(b2).to be > b1
    end

    it "ジャンルが同じときはタイトルで並ぶこと" do
      b1 = Book.new("1","1","a")
      b2 = Book.new("2","1","a")
      expect(b1).to be < b2
    end

    it "ジャンルとタイトルが同じときは同一であるとみなすこと" do
      b1 = Book.new("1","1","a")
      b2 = Book.new("1","2","a")
      expect(b2).to be == b1
      expect(b1).to be == b2
    end
  end
end